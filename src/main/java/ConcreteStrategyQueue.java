import java.util.ArrayList;
import java.util.concurrent.LinkedBlockingQueue;

public class ConcreteStrategyQueue implements Strategy {

    @Override
    public int addTask(ArrayList<Server> servers, Task t) {
        int j = 0, k = 1, mLength = 100;
        LinkedBlockingQueue<Task> tasks;
        for (Server i : servers) {
            tasks = i.getTasks();
            if(tasks.size() == 0) {
                i.addTask(t);
                return i.getTime();
            }
            else if(tasks.size() < mLength) {
                k = j;
                mLength = tasks.size();
            }
            j++;
        }
        servers.get(k).addTask(t);
        return servers.get(k).getTime();
    }
}
