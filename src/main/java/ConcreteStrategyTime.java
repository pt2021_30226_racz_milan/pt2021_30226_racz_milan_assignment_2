import java.util.ArrayList;

public class ConcreteStrategyTime implements Strategy{

    @Override
    public int addTask(ArrayList<Server> servers, Task t) {
        int j = 0, k = 1, time = 10000;
        for (Server i : servers) {
            if(i.getTime() == 0) {
                i.addTask(t);
                return i.getTime();
            }
            else if(i.getTime() < time) {
                k = j;
                time = i.getTime();
            }
            j++;
        }
        servers.get(k).addTask(t);
        return servers.get(k).getTime();
    }
}
