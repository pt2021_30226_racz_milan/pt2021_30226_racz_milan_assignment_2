import java.awt.Dimension;
import java.awt.FlowLayout;
import java.io.*;
import java.util.ArrayList;
import java.util.concurrent.LinkedBlockingQueue;

import javax.swing.Box;
import javax.swing.BoxLayout;
import javax.swing.JFrame;
import javax.swing.JLabel;
import javax.swing.JPanel;
import javax.swing.JTextArea;

public class Frame {
    private Scheduler scheduler;
    private ArrayList<Server> servers;
    public JLabel l1 = new JLabel ("Time: ");
    public JLabel l2 = new JLabel ("Waiting clients: ");
    public JLabel l3 = new JLabel ("Queues: ");
    public JTextArea tf1 = new JTextArea(1,2);
    public JTextArea tf2 = new JTextArea(3,30);
    public JTextArea tf3 = new JTextArea(10,30);

    public Frame(Scheduler scheduler) {
        this.scheduler = scheduler;
        servers = this.scheduler.getServers();

        JFrame frame = new JFrame ("Queue simulator");
        frame.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
        frame.setSize(360, 400);

        JPanel panel1 = new JPanel();
        JPanel panel2 = new JPanel();
        JPanel panel3 = new JPanel();
        JPanel panel4 = new JPanel();
        JPanel panel5 = new JPanel();
        JPanel panel6 = new JPanel();

        FlowLayout flLayout = new FlowLayout();

        panel1.setLayout(new BoxLayout(panel1, BoxLayout.Y_AXIS));
        panel2.setLayout(flLayout);
        panel3.setLayout(flLayout);
        panel4.setLayout(flLayout);
        panel5.setLayout(flLayout);
        panel6.setLayout(flLayout);

        panel1.add( Box.createRigidArea(new Dimension(0,5)) );

        panel2.add(l1);
        panel2.add(tf1);
        panel3.add(l2);
        panel4.add(tf2);
        panel5.add(l3);
        panel6.add(tf3);

        panel1.add(panel2);
        panel1.add( Box.createRigidArea(new Dimension(0,5)) );

        panel1.add(panel3);
        panel1.add( Box.createRigidArea(new Dimension(0,5)) );

        panel1.add(panel4);
        panel1.add( Box.createRigidArea(new Dimension(0,5)) );

        panel1.add(panel5);
        panel1.add( Box.createRigidArea(new Dimension(0,5)) );

        panel1.add(panel6);
        panel1.add( Box.createRigidArea(new Dimension(0,5)) );

        frame.setContentPane(panel1);
        frame.setVisible(true);
    }

    public void show(ArrayList<Task> tasks, int curT) {
        int counter = 0;
        String s1 = "";
        String s2 = "";
        String s3 = "";
        s1 += curT;
        for(Task i : tasks) {
            s2 += "("+i.getId()+","+i.getArr()+","+i.getProc()+"); ";
            counter++;
            if(counter == 7) {
                s2 += "\n";
                counter = 0;
            }
        }
        int j = 1;
        for(Server k : servers) {
            s3 += "Queue " + j + ": ";
            LinkedBlockingQueue<Task> t = k.getTasks();
            for(Task i : t) {
                s3 += "("+i.getId()+","+i.getArr()+","+i.getProc()+"); ";
            }
            s3 += "\n";
            j++;
        }
        String s = "Time: "+s1+"\n"+"Waiting clients: "+s2+"\n"+"Queues:\n"+s3+"\n";
        writeFile(s);
        tf1.setText(s1);
        tf2.setText(s2);
        tf3.setText(s3);
    }

    public void writeFile(String s) {
        try {
            FileWriter fstream = new FileWriter("log.txt",true);
            BufferedWriter out = new BufferedWriter(fstream);
            out.write(s);
            out.close();
        } catch (IOException e) {
            System.out.println("An error occurred.");
        }
    }

    public void showResults(double averageWaitingTime, double averageServiceTime, int peakHour) {
        String s3 = "";

        s3 += "Average waiting time = " + averageWaitingTime + "\n";
        s3 += "Average service time = " + averageServiceTime + "\n";
        s3 += "Peak hour = " + peakHour;

        writeFile(s3);
        tf3.setText(s3);
    }
}
