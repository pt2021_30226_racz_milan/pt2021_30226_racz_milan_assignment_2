import java.util.ArrayList;
import java.util.concurrent.LinkedBlockingQueue;


public class Scheduler {
    private ArrayList<Server> servers;
    private int maxNoServers;
    private int totalWaitingTime;
    private Strategy strategy;

    public Scheduler (int maxNoServers, int finalT) {
        this.maxNoServers = maxNoServers;
        servers = new ArrayList<Server>();
        for(int i = 0; i < maxNoServers; i++) {
            Server s = new Server(finalT);
            Thread t = new Thread(s);
            t.start();
            servers.add(s);
        }
    }

    public void changeStrategy(SelectionPolicy policy) {
        if(policy == SelectionPolicy.SHORTEST_TIME) {
            strategy = new ConcreteStrategyTime();
        }
        if(policy == SelectionPolicy.SHORTEST_QUEUE) {
            strategy = new ConcreteStrategyQueue();
        }
    }

    public void dispatchTask(Task t) {
        addToTotalWaitingTime(strategy.addTask(servers, t));
    }

    public ArrayList<Server> getServers(){
        return servers;
    }

    public int getClientNumberInQueues() {
        int x = 0;
        for(Server i : servers) {
            LinkedBlockingQueue<Task> tasks = i.getTasks();
            for(Task j : tasks)
                x++;
        }
        return x;
    }

    public void addToTotalWaitingTime(int t) {
        totalWaitingTime += t;
    }

    public int getTotalWaitingTime() {
        return totalWaitingTime;
    }

    public int getNoServers() {
        return maxNoServers;
    }
}