import java.util.concurrent.LinkedBlockingQueue;
import java.util.concurrent.atomic.AtomicInteger;

public class Server implements Runnable {

    private LinkedBlockingQueue<Task> tasks;
    private AtomicInteger waitingT;
    private AtomicInteger finalT;

    public Server(int finalT) {
        tasks = new LinkedBlockingQueue<Task>();
        waitingT = new AtomicInteger(0);
        this.finalT = new AtomicInteger(finalT);
    }

    public void addTask (Task newTask) {
        tasks.add(newTask);
        waitingT.addAndGet(newTask.getProc());
    }

    @Override
    public void run() {
        int j = 0;
        while(j <= finalT.get()) {
            Task t = tasks.peek();
            if(t != null) {
                t.setProc(t.getProc() - 1);
                waitingT.decrementAndGet();
                if(t.getProc() == 0)
                    tasks.poll();
            }
            try {
                Thread.sleep(998);
            }catch (InterruptedException e) {
                System.out.println("Interruption");
                return;
            }
            j++;
        }
    }

    public LinkedBlockingQueue<Task> getTasks() {
        return tasks;
    }

    public int getTime() {
        return waitingT.get();
    }
}