import java.awt.Dimension;
import java.awt.FlowLayout;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.io.*;
import java.io.IOException;

import javax.swing.Box;
import javax.swing.BoxLayout;
import javax.swing.JButton;
import javax.swing.JFrame;
import javax.swing.JLabel;
import javax.swing.JPanel;
import javax.swing.JTextArea;

public class Setup implements ActionListener {
    public JLabel l1 = new JLabel ("Time limit: ");
    public JLabel l2 = new JLabel ("Minimum and maximum processing time: ");
    public JLabel l3 = new JLabel ("Minimum and maximum arrival time: ");
    public JLabel l4 = new JLabel ("Number of servers: ");
    public JLabel l5 = new JLabel ("Number of clients: ");
    public JTextArea tf1 = new JTextArea(1,2);
    public JTextArea tf2 = new JTextArea(1,2);
    public JTextArea tf3 = new JTextArea(1,2);
    public JTextArea tf4 = new JTextArea(1,2);
    public JTextArea tf5 = new JTextArea(1,2);
    public JTextArea tf6 = new JTextArea(1,2);
    public JTextArea tf7 = new JTextArea(1,2);
    public JButton b = new JButton("Start");

    public Setup() {

        b.addActionListener(this);
        JFrame frame = new JFrame ("Simulation setup");
        frame.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
        frame.setSize(320, 250);

        JPanel panel1 = new JPanel();
        JPanel panel2 = new JPanel();
        JPanel panel3 = new JPanel();
        JPanel panel4 = new JPanel();
        JPanel panel5 = new JPanel();
        JPanel panel6 = new JPanel();
        JPanel panel7 = new JPanel();

        FlowLayout flLayout = new FlowLayout();

        panel1.setLayout(new BoxLayout(panel1, BoxLayout.Y_AXIS));
        panel2.setLayout(flLayout);
        panel3.setLayout(flLayout);
        panel4.setLayout(flLayout);
        panel5.setLayout(flLayout);
        panel6.setLayout(flLayout);
        panel7.setLayout(flLayout);

        panel1.add( Box.createRigidArea(new Dimension(0,5)) );

        panel2.add(l1);
        panel2.add(tf1);
        panel3.add(l2);
        panel3.add(tf2);
        panel3.add(tf3);
        panel4.add(l3);
        panel4.add(tf4);
        panel4.add(tf5);
        panel5.add(l4);
        panel5.add(tf6);
        panel6.add(l5);
        panel6.add(tf7);
        panel7.add(b);

        panel1.add(panel2);
        panel1.add( Box.createRigidArea(new Dimension(0,5)) );

        panel1.add(panel3);
        panel1.add( Box.createRigidArea(new Dimension(0,5)) );

        panel1.add(panel4);
        panel1.add( Box.createRigidArea(new Dimension(0,5)) );

        panel1.add(panel5);
        panel1.add( Box.createRigidArea(new Dimension(0,5)) );

        panel1.add(panel6);
        panel1.add( Box.createRigidArea(new Dimension(0,5)) );

        panel1.add(panel7);
        panel1.add( Box.createRigidArea(new Dimension(0,5)) );

        frame.setContentPane(panel1);
        frame.setVisible(true);
    }

    public void createLog() {
        try {
            File file = new File("log.txt");
            file.createNewFile();
            FileWriter f = new FileWriter("log.txt",false);
            f.write("Simulation:\n");
            f.close();
        } catch (IOException e) {
            System.out.println("An error occurred.");
        }
    }

    public void actionPerformed(ActionEvent e) {
        createLog();

        String in1, in2, in3, in4, in5, in6, in7;
        int time, minProc, maxProc, minArr, maxArr, servers, clients;
        in1 = getFirst();
        in2 = getSecond();
        in3 = getThird();
        in4 = getFourth();
        in5 = getFifth();
        in6 = getSixth();
        in7 = getSeventh();

        time = Integer.parseInt(in1);
        minProc = Integer.parseInt(in2);
        maxProc = Integer.parseInt(in3);
        minArr = Integer.parseInt(in4);
        maxArr = Integer.parseInt(in5);
        servers = Integer.parseInt(in6);
        clients = Integer.parseInt(in7);

        SimulationManager m = new SimulationManager(time, minProc, maxProc, minArr, maxArr, servers, clients);
        Thread t = new Thread(m);
        t.start();
    }

    public String getFirst() {
        return tf1.getText();
    }
    public String getSecond() {
        return tf2.getText();
    }
    public String getThird() {
        return tf3.getText();
    }
    public String getFourth() {
        return tf4.getText();
    }
    public String getFifth() {
        return tf5.getText();
    }
    public String getSixth() {
        return tf6.getText();
    }
    public String getSeventh() {
        return tf7.getText();
    }
}
