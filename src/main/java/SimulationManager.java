import java.util.ArrayList;
import java.util.Collections;

public class SimulationManager implements Runnable{
    public int timeLimit = 0;
    public int maxProcT = 0;
    public int minProcT = 0;
    public int maxArrT = 0;
    public int minArrT = 0;
    public int noServers = 0;
    public int noClients = 0;
    public SelectionPolicy selectPol = SelectionPolicy.SHORTEST_TIME;

    private Scheduler scheduler;
    private Frame frame;
    private ArrayList<Task> tasks;

    public SimulationManager(int timeLimit, int minProcT, int maxProcT, int minArrT, int maxArrT, int noServers, int noClients) {
        this.timeLimit = timeLimit;
        this.maxProcT = maxProcT;
        this.minProcT = minProcT;
        this.maxArrT = maxArrT;
        this.minArrT = minArrT;
        this.noServers = noServers;
        this.noClients = noClients;

        scheduler = new Scheduler(noServers, timeLimit + 1);
        scheduler.changeStrategy(selectPol);
        tasks = new ArrayList<Task>();
        frame = new Frame(scheduler);
        generateTasks();
    }

    private double getTotalServiceTime() {
        double s = 0;
        for(Task i : tasks)
            s += i.getProc();
        return s;
    }

    private void generateTasks() {
        int a, p;
        for(int i = 0; i < noClients; i++) {
            p = (int) Math.floor(Math.random() * (maxProcT - minProcT - 1) + minProcT + 1);
            a = (int) Math.floor(Math.random() * (maxArrT - minArrT - 1) + minArrT + 1);
            tasks.add(new Task(i, a, p));
        }
        Collections.sort(tasks);
    }

    @Override
    public void run() {
        int currentTime = 0, j = 0, maxClientNumber = 0, peakHour = 0;
        double averageWaitingTime = 0, averageServiceTime = 0;
        averageServiceTime = 1.0 * getTotalServiceTime() / noClients;
        while(currentTime <= timeLimit) {
            for(Task i : tasks)
                if(i.getArr() == currentTime) {
                    scheduler.dispatchTask(i);
                    j++;
                }
            while(j > 0) {
                tasks.remove(0);
                j--;
            }
            if(scheduler.getClientNumberInQueues() > maxClientNumber) {
                maxClientNumber = scheduler.getClientNumberInQueues();
                peakHour = currentTime;
            }
            frame.show(tasks, currentTime);
            currentTime++;
            try {
                Thread.sleep(1000);
            } catch (InterruptedException e) {
                System.out.println("Interruption");
                return;
            }
        }
        averageWaitingTime = (1.0 * scheduler.getTotalWaitingTime() / noClients);
        frame.showResults(averageWaitingTime, averageServiceTime, peakHour);
    }

}
