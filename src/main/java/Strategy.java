import java.util.ArrayList;

public interface Strategy {
    public int addTask(ArrayList<Server> servers, Task t);
}