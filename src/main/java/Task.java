public class Task implements Comparable<Object>{
    private int id;
    private int arrivalT;
    private int processingT;

    public Task(int id, int arrivalT, int processingT) {
        this.id = id;
        this.arrivalT = arrivalT;
        this.processingT = processingT;
    }

    public void setProc(int processingT) {
        this.processingT = processingT;
    }

    public int getId() {
        return id;
    }

    public int getArr() {
        return arrivalT;
    }

    public int getProc() {
        return processingT;
    }

    @Override
    public int compareTo(Object comp) {
        int compareArrivalT = ((Task)comp).getArr();
        return this.arrivalT - compareArrivalT;
    }
}
